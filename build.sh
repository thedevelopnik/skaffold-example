#! /usr/bin/env bash

pack build \
  registry.gitlab.com/thedevelopnik/skaffold-example:dev \
  --builder heroku/buildpacks:18 \
  --buildpack heroku/go \
  --buildpack heroku/procfile \
  --path .
