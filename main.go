package main

import (
	"fmt"
	"net/http"
)

func main() {
	fmt.Println("Starting up!")

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Let's get back to skaffolding!")
	})

	fmt.Println("Listening on port 3000!")

	http.ListenAndServe(":3000", nil)
}
