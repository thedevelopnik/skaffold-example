# skaffold-example

This repo shows how to use [pack](https://buildpacks.io), [helm](https://helm.sh) and [skaffold](https://skaffold.dev) together
to create an efficient setup to do "local" development against a kubernetes cluster.

Pack allows you to skip writing your own Dockerfile. Helm, when used in an organization that maintains common helm charts,
allows you to skip writing your own deployment definitions (though this repo contains an example chart). Skaffold, through
its `$ skaffold dev` command, leverages those tools to hotload your code into a cluster.

## Pack

To use pack to build a container image for this app, we'll use Heroku's public buildpacks based on Ubuntu 18.04.
The command outside of Skaffold is:

```shell
$ pack build registry.gitlab.com/thedevelopnik/skaffold-example \
  --builder heroku/buildpacks:18 \
  --buildpack heroku/go \
  --path .
```

We aren't using any private git repos as dependencies in this app, but the Heroku buildpack supports that use case via an environment variable. 

## Helm
  
We've created a helm chart using `$ helm create example-chart`, and removed parts of the generic template we don't need,
like the horizontal pod autoscaler and service account.

Additionally, I run the [Ambassador API gateway](https://www.getambassador.io) in all my clusters to handle incoming requests, so I've removed the default Ingress object that helm creates and replaced it with an Ambassador `mapping`.

An example helm command is

```shell
$ helm install <chartrepo>/<chartname> \
  --namespace <mynamespace> \
  --name mygreatapp \
  -f path/to/localdevvalues.yaml
```

## Skaffold
Finally, we have a skaffold.yml file, which connects our `pack` and `helm` resources to hotload our application into the cluster
on changes to the code.

We can deploy and iterate in the development process using `$ skaffold dev` and then in a CI/CD pipeline use `$ skaffold run` to do a final deployment.

## Checking It Out
This application is running live, right now, having been built and deployed with these tools! You can hit it at https://apps.thedevelopnik.com/skaffold
